def file_shuffler
  puts "what file would you like to shuffle?"
  file_name = gets.chomp
  lines = File.readlines(file_name)
  shuffled = lines.shuffle

  File.open("#{file_name}-shuffle.txt", "w") do |f|
    shuffled.each { |line| f.puts line }
  end

end
#
if __FILE__ == $PROGRAM_NAME
  file_shuffler
end
